package com.tes.echelon.handler;

import com.tes.echelon.utility.ADData;

public class SetWorkoutControlStateCmd extends Command {
  int mState = 0;
  
  public SetWorkoutControlStateCmd(int paramInt) {
    this.mState = paramInt;
  }
  
  public ADData compactRequestData() {
    this.commandData.appendByte((byte)this.mState);
    return super.compactRequestData();
  }
  
  protected byte getActionCode() {
    return -80;
  }
}


/* Location:              /home/sakura/work/test/dex2jar-2.1/dex-tools-2.1-SNAPSHOT/classes-dex2jar.jar!/changyow/santoble/handler/SetWorkoutControlStateCmd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */