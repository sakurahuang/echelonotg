package com.tes.echelon.handler;


import com.tes.echelon.listener.BLEPeripheralListener;
import com.tes.echelon.util.Workout;
import com.tes.echelon.utility.ADConverter;
import com.tes.echelon.utility.ADData;
import com.tes.echelon.utility.ADLog;

public class GetResistanceLevelCmd extends Command {
    protected byte getActionCode() {
        return -91;
    }

    public void handleReceivedData(ADData paramADData, final BLEPeripheralListener listener) {
        byte[] arrayOfByte = paramADData.bytes();
        ADLog.i(getClass().getName(), "handleReceivedData %s", new Object[]{ADConverter.byteArrayToHexString(arrayOfByte)});
        final int level = toInteger(arrayOfByte[3]);
        Workout.sharedInstance().setResistanceLevel(level);
        if (listener != null)
            Command.post(new Runnable() {
                public void run() {
                    listener.onGetResistanceLevelResponse(level);
                }
            });
    }
}


/* Location:              /home/sakura/work/test/dex2jar-2.1/dex-tools-2.1-SNAPSHOT/classes-dex2jar.jar!/changyow/santoble/handler/GetResistanceLevelCmd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */