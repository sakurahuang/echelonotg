package com.tes.echelon.handler;

import com.tes.echelon.listener.BLEPeripheralListener;
import com.tes.echelon.utility.ADConverter;
import com.tes.echelon.utility.ADData;
import com.tes.echelon.utility.ADLog;

import java.util.Arrays;

public class GetErrorLogCmd extends Command {
    protected byte getActionCode() {
        return -94;
    }

    public void handleReceivedData(ADData paramADData, final BLEPeripheralListener listener) {
        final byte[] errorCodes = paramADData.bytes();
        ADLog.i(getClass().getName(), "handleReceivedData %s", new Object[]{ADConverter.byteArrayToHexString(errorCodes)});
       byte[]  arrayBytes = Arrays.copyOfRange(errorCodes, 3, errorCodes.length - 1);
        if (listener != null)
            Command.post(new Runnable() {
                public void run() {
                    listener.onGetErrorLogResponse(arrayBytes);
                }
            });
    }
}


/* Location:              /home/sakura/work/test/dex2jar-2.1/dex-tools-2.1-SNAPSHOT/classes-dex2jar.jar!/changyow/santoble/handler/GetErrorLogCmd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */