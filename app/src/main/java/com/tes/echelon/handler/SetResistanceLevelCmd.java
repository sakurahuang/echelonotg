package com.tes.echelon.handler;

import com.tes.echelon.utility.ADData;

public class SetResistanceLevelCmd extends Command {
  int mResistanceLevel = 0;
  
  public SetResistanceLevelCmd(int paramInt) {
    this.mResistanceLevel = paramInt;
  }
  
  public ADData compactRequestData() {
    this.commandData.appendByte((byte)this.mResistanceLevel);
    return super.compactRequestData();
  }
  
  protected byte getActionCode() {
    return -79;
  }
}


/* Location:              /home/sakura/work/test/dex2jar-2.1/dex-tools-2.1-SNAPSHOT/classes-dex2jar.jar!/changyow/santoble/handler/SetResistanceLevelCmd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */