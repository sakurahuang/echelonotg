package com.tes.echelon.handler;

import com.tes.echelon.listener.BLEPeripheralListener;
import com.tes.echelon.utility.ADConverter;
import com.tes.echelon.utility.ADData;
import com.tes.echelon.utility.ADLog;


public class GetWorkoutControlStateCmd extends Command {
    protected byte getActionCode() {
        return -92;
    }

    public void handleReceivedData(ADData paramADData, final BLEPeripheralListener listener) {
        byte[] arrayOfByte = paramADData.bytes();
        ADLog.i(getClass().getName(), "handleReceivedData %s", new Object[]{ADConverter.byteArrayToHexString(arrayOfByte)});
        final int state = toInteger(arrayOfByte[3]);
        if (listener != null)
            Command.post(new Runnable() {
                public void run() {
                    listener.onGetControlStateResponse(state);
                }
            });
    }
}


/* Location:              /home/sakura/work/test/dex2jar-2.1/dex-tools-2.1-SNAPSHOT/classes-dex2jar.jar!/changyow/santoble/handler/GetWorkoutControlStateCmd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */