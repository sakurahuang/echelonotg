package com.tes.echelon.handler;

import com.tes.echelon.listener.BLEPeripheralListener;
import com.tes.echelon.util.Workout;
import com.tes.echelon.utility.ADConverter;
import com.tes.echelon.utility.ADData;
import com.tes.echelon.utility.ADLog;

public class WorkoutStatusNotify extends Command {
    protected byte getActionCode() {
        return -47;
    }

    public void handleReceivedData(ADData paramADData, final BLEPeripheralListener listener) {
        byte[] arrayOfByte = paramADData.bytes();
        ADLog.i(getClass().getName(), "handleReceivedData %s", new Object[]{ADConverter.byteArrayToHexString(arrayOfByte)});
        byte b2 = arrayOfByte[3];
        byte timestamp = arrayOfByte[4];
        final int count = arrayOfByte[5] << 24 & 0xFF000000 | arrayOfByte[6] << 16 & 0xFF0000 | arrayOfByte[7] << 8 & 0xFF00 | arrayOfByte[8] << 0 & 0xFF;
        final int rpm = arrayOfByte[9] << 8 & 0xFF00 | arrayOfByte[10] << 0 & 0xFF;
        byte hr = arrayOfByte[11];
        double speed = Workout.getSpeedMultiplier();
        double distance = Workout.getDistancePerCount();
        final double watt = Workout.sharedInstance().getWatt(b2);
        if (Workout.sharedInstance().getPreviousCount() > b2)
            Workout.sharedInstance().resetWorkout();
        Workout.sharedInstance().addCaloriesWithCurrentCount(b2);
        final double calories = Workout.sharedInstance().getCalories();
        if (listener != null)
            Command.post(new Runnable() {
                public void run() {
                    listener.workoutStatusChanged(timestamp, count, rpm, hr, speed, distance, calories, watt);
                }
            });
    }
}


/* Location:              /home/sakura/work/test/dex2jar-2.1/dex-tools-2.1-SNAPSHOT/classes-dex2jar.jar!/changyow/santoble/handler/WorkoutStatusNotify.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */