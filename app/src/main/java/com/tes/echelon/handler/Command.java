package com.tes.echelon.handler;

import android.os.Handler;
import android.os.Message;

import androidx.annotation.NonNull;

import com.tes.echelon.listener.BLEPeripheralListener;
import com.tes.echelon.util.ChecksumUtil;
import com.tes.echelon.utility.ADData;

public abstract class Command {
  public static Handler shared_handler=new Handler(){
    @Override
    public void handleMessage(@NonNull Message msg) {
      super.handleMessage(msg);

    }
  };
  
  protected ADData commandData = new ADData();
  
  public static void post(Runnable paramRunnable) {
    shared_handler.postDelayed(paramRunnable,10);
  }
  
  public ADData compactRequestData() {
    ADData aDData = new ADData();
    aDData.appendByte((byte)-16);
    aDData.appendByte(getActionCode());
    aDData.appendByte((byte)this.commandData.length());
    aDData.appendBytes(this.commandData.bytes());
    aDData.appendByte(ChecksumUtil.calcChecksum(aDData.bytes()));
    return aDData;
  }
  
  protected byte getActionCode() {
    return 0;
  }
  
  public void handleReceivedData(ADData paramADData, BLEPeripheralListener paramBLEPeripheralListener) {}
  
  public boolean isExpectedData(ADData paramADData) {
    return isExpectedData(paramADData, true);
  }
  
  public boolean isExpectedData(ADData paramADData, boolean paramBoolean) {
    int i = paramADData.length();
    byte[] arrayOfByte = paramADData.bytes();
    if (i != arrayOfByte[2] + 4)
      return false; 
    if (paramBoolean && !ChecksumUtil.verifyChecksum(arrayOfByte))
      return false; 
    i = getActionCode();
    paramBoolean = true;
    if (i != arrayOfByte[1])
      paramBoolean = false; 
    return paramBoolean;
  }
  
  protected byte toByte(int paramInt) {
    return (byte)(paramInt % 256 & 0xFF);
  }
  
  protected byte toByte(long paramLong) {
    return (byte)(int)(paramLong % 256L & 0xFFL);
  }
  
  protected int toInteger(byte paramByte) {
    return toInteger((byte)0, (byte)0, (byte)0, paramByte);
  }
  
  protected int toInteger(byte paramByte1, byte paramByte2) {
    return toInteger((byte)0, (byte)0, paramByte1, paramByte2);
  }
  
  protected int toInteger(byte paramByte1, byte paramByte2, byte paramByte3) {
    return toInteger((byte)0, paramByte1, paramByte2, paramByte3);
  }
  
  protected int toInteger(byte paramByte1, byte paramByte2, byte paramByte3, byte paramByte4) {
    return (paramByte1 & 0xFF) << 24 | 0x0 | (paramByte2 & 0xFF) << 16 | (paramByte3 & 0xFF) << 8 | paramByte4 & 0xFF;
  }
}

