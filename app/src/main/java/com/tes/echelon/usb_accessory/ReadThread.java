package com.tes.echelon.usb_accessory;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.tes.echelon.utility.ADConverter;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;

import okio.Buffer;

class ReadThread extends Thread {
    final String TAG = "ReadThread";

    final int USB_DATA_BUFFER = 8192;

    boolean bReadTheadEnable = false;

    Handler mHandler = new Handler(Looper.getMainLooper());

    FileInputStream mInputStream;

    ReadThreadListener mListener;

    ReadThread(ReadThreadListener paramReadThreadListener, FileInputStream paramFileInputStream) {
        this.mListener = paramReadThreadListener;
        this.mInputStream = paramFileInputStream;
        this.bReadTheadEnable = true;
        setPriority(10);
    }

    public void run() {
        byte[] arrayOfByte = new byte[8192];
        System.out.println("=====ReadThread======"+bReadTheadEnable);
        while (true == bReadTheadEnable) {
            if (mInputStream != null) {
                boolean bool = false;
                int count = 0;
                try {
                    Log.i("ReadThread", "wait read...");
                    count = mInputStream.read(arrayOfByte, 0, 8192);
                    if (count > 0) {
                        bool = true;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    bool = false;
                }
                if (!bool) {
                    Log.e("ReadThread", "read USB data error!");
                    ReadThreadListener readThreadListener = this.mListener;
                    if (readThreadListener != null)
                        readThreadListener.onReadErrorOccur();
                    this.bReadTheadEnable = false;
                }
                if (bool) {
                    final Buffer buffer = new Buffer();
                    buffer.write(arrayOfByte, 0, count);
                    this.mHandler.post(new Runnable() {
                        public void run() {
                            ReadThread.this.mListener.onReadData(buffer);
                        }
                    });
                }

            }
        }

    }

    public void stopRead() {
        this.bReadTheadEnable = false;
    }
}


/* Location:              /home/sakura/work/test/dex2jar-2.1/dex-tools-2.1-SNAPSHOT/classes-dex2jar.jar!/changyow/santoble/usb_accessory/ReadThread.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */