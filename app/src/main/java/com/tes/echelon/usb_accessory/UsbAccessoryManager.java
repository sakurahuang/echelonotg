package com.tes.echelon.usb_accessory;

import android.hardware.usb.UsbAccessory;
import android.hardware.usb.UsbManager;
import android.os.Handler;
import android.os.Looper;
import android.os.ParcelFileDescriptor;

import androidx.annotation.NonNull;

import com.tes.echelon.handler.AckCmd;
import com.tes.echelon.handler.Command;
import com.tes.echelon.handler.GetDeviceInfoCmd;
import com.tes.echelon.handler.GetErrorLogCmd;
import com.tes.echelon.handler.GetResistanceLevelCmd;
import com.tes.echelon.handler.GetResistanceLevelRangeCmd;
import com.tes.echelon.handler.GetWorkoutControlStateCmd;
import com.tes.echelon.handler.ResistanceLevelNotify;
import com.tes.echelon.handler.SetResistanceLevelCmd;
import com.tes.echelon.handler.SetWorkoutControlStateCmd;
import com.tes.echelon.handler.WorkoutControlStateNotify;
import com.tes.echelon.handler.WorkoutStatusNotify;
import com.tes.echelon.handler.factory.GetSleepSettingCmd;
import com.tes.echelon.handler.factory.SetSleepSettingCmd;
import com.tes.echelon.util.ChecksumUtil;
import com.tes.echelon.utility.ADConverter;
import com.tes.echelon.utility.ADData;
import com.tes.echelon.utility.ADLog;

import java.io.EOFException;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.CopyOnWriteArrayList;

import okio.Buffer;


public class UsbAccessoryManager {
    private static final long SEND_COMMAND_INTERVAL = 1000L;

    private static UsbAccessoryManager Shared_Instance;

    private static String TAG = "UsbAccessoryManager";

    private boolean bReadLock = false;

    protected final CopyOnWriteArrayList<Command> mCommands = new CopyOnWriteArrayList<Command>();

    ParcelFileDescriptor mFileDescriptor;

    Handler mHandler = new Handler(Looper.getMainLooper());

    private boolean mInitialized = false;

    FileInputStream mInputStream;

    private UsbAccessoryListerer mListener = null;

    FileOutputStream mOutputStream;

    private final Buffer mReadBuffer = new okio.Buffer();

    ReadThread mReadThread;

    ReadThreadListener mReadThreadListener = new ReadThreadListener() {
        public void onReadData(Buffer param1Buffer) {
            byte[] arrayOfByte = param1Buffer.readByteArray();
            ADLog.i(UsbAccessoryManager.TAG, "RCV %s", new Object[]{ADConverter.byteArrayToHexString(arrayOfByte)});
            if (!UsbAccessoryManager.this.mInitialized) {
                UsbAccessoryManager.this.mInitialized = true;
                try {
                    UsbAccessoryManager.this.mListener.peripheralInitialized();
                } catch (RuntimeException runtimeException) {
                    ADLog.e(UsbAccessoryManager.TAG, "Unexpected exception in bleCentralManagerDidInitialized", new Object[0]);
                    UsbAccessoryManager.this.setListener(null);
                }
                getDeviceInfo();
            }
            UsbAccessoryManager.this.mReadBuffer.write(arrayOfByte);
            UsbAccessoryManager.this.processReadData();
        }

        public void onReadErrorOccur() {
            try {
                mListener.onReadErrorOccur();
            } catch (RuntimeException runtimeException) {
                ADLog.e(UsbAccessoryManager.TAG, "Unexpected exception in bleCentralManagerDidInitialized", new Object[0]);
                UsbAccessoryManager.this.setListener(null);
            }
            try {
                UsbAccessoryManager.this.destoryAccessory(true);
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        }
    };


    Runnable mSendCmdRunnable = new Runnable() {
        public void run() {
            if (UsbAccessoryManager.this.mUsbAccessory != null && UsbAccessoryManager.this.mOutputStream != null)
                UsbAccessoryManager.this.ack();
            UsbAccessoryManager.this.mHandler.removeCallbacks(UsbAccessoryManager.this.mSendCmdRunnable);
//            mHandler.postDelayed(mSendCmdRunnable, 1000L);
        }
    };

    public void ack() {
        writeValue((Command) new AckCmd());
    }

    UsbAccessoryListerer mUsbAListener = null;

    UsbAccessory mUsbAccessory = null;

    UsbManager mUsbManager = null;

    byte[] mWriteDataBuffer;

    public StringBuffer rcvLog = new StringBuffer();

    SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss.SSS");

    protected UsbAccessoryManager(@NonNull UsbManager paramUsbManager, @NonNull UsbAccessory paramUsbAccessory) {
        this.mUsbManager = paramUsbManager;
        this.mUsbAccessory = paramUsbAccessory;
        init();
    }

    public static UsbAccessoryManager createInstance(@NonNull UsbManager paramUsbManager, @NonNull UsbAccessory paramUsbAccessory) {
        Shared_Instance = new UsbAccessoryManager(paramUsbManager, paramUsbAccessory);
        return Shared_Instance;
    }

    public static UsbAccessoryManager getInstance() {
        UsbAccessoryManager usbAccessoryManager = Shared_Instance;
        if (usbAccessoryManager != null)
            return usbAccessoryManager;
        throw new IllegalArgumentException("Call 'createInstance' before geting instance.");
    }

    private void init() {
        this.mWriteDataBuffer = new byte[2048];
        this.mInputStream = null;
        this.mOutputStream = null;
    }

    public void getDeviceInfo() {
        writeValue(new GetDeviceInfoCmd());
    }

    public void getErrorLog() {
        writeValue(new GetErrorLogCmd());
    }

    public void getResistanceLevel() {
        writeValue(new GetResistanceLevelCmd());
    }

    public void getResistanceLevelRange() {
        writeValue(new GetResistanceLevelRangeCmd());
    }

    public void getWorkoutControlState() {
        writeValue(new GetWorkoutControlStateCmd());
    }

    public void stopWorkout() {
        setWorkoutControlState(0);
    }

    private void setWorkoutControlState(int paramInt) {
        writeValue(new SetWorkoutControlStateCmd(paramInt));
    }

    public void startWorkout() {
        setWorkoutControlState(1);
    }

    public void setResistanceLevel(int paramInt) {
        writeValue(new SetResistanceLevelCmd(paramInt));
    }

    public void getSleepSetting() {
        writeValue(new GetSleepSettingCmd());
    }

    public void setSleepSetting(int paramInt) {
        if (paramInt == 0) {
            paramInt = 0;
        } else {
            paramInt = 1;
        }
        writeValue((Command) new SetSleepSettingCmd(paramInt));
    }


    public static boolean isInitialized() {
        UsbAccessoryManager usbAccessoryManager = Shared_Instance;
        return (usbAccessoryManager == null) ? false : usbAccessoryManager.mInitialized;
    }

    private void processCommand(ADData paramADData) {
        for (Command command : this.mCommands) {
            if (command.isExpectedData(paramADData, false))
                command.handleReceivedData(paramADData, this.mListener);
        }
    }

    private void processReadData() {
        synchronized (this.mReadBuffer) {
            if (!this.bReadLock) {
                boolean bool;
                if (!this.bReadLock) {
                    bool = true;
                } else {
                    bool = false;
                }
                this.bReadLock = bool;
                long l = this.mReadBuffer.size();
                while (l >= 4L) {
                    long l1 = (this.mReadBuffer.getByte(2L) + 4);
                    if (l < l1)
                        break;
                    try {
                        this.mReadBuffer.copyTo(mOutputStream, 0L, l1);
                    } catch (Exception e) {

                    }

                    ADData aDData = new ADData(mReadBuffer.readByteArray());
                    if (ChecksumUtil.verifyChecksum(aDData.bytes())) {
                        processCommand(aDData);
                        try {
                            mReadBuffer.skip(l1);
                        } catch (EOFException eOFException) {
                            eOFException.printStackTrace();
                        }
                    } else {
                        try {
                            mReadBuffer.skip(1L);
                        } catch (EOFException eOFException) {
                            eOFException.printStackTrace();
                        }
                    }
                    l = this.mReadBuffer.size();
                    mReadBuffer.clear();
                }
                synchronized (this.mReadBuffer) {
                    this.bReadLock = false;
                    return;
                }
            }
            return;
        }
    }


    private void sendPacket(int paramInt) {
        try {
            if (mOutputStream != null) {
                mOutputStream.write(mWriteDataBuffer, 0, paramInt);
            }
        } catch (IOException iOException) {
            iOException.printStackTrace();
        }
    }

    public void destoryAccessory(boolean bConfiged) {
        try {
            if (this.mReadThread != null)
                this.mReadThread.stopRead();
        } catch (Exception e) {
            e.printStackTrace();
        }
        mWriteDataBuffer[0] = 0;
        sendPacket(1);
        try {
            Thread.sleep(10);
        } catch (Exception e) {

        }
        closeAccessory();
    }


    private void closeAccessory() {
        try {
            if (mFileDescriptor != null)
                mFileDescriptor.close();
        } catch (IOException iOException) {
            iOException.printStackTrace();
        }
        try {
            if (mInputStream != null)
                mInputStream.close();
        } catch (IOException iOException) {
            iOException.printStackTrace();
        }
        try {
            if (mOutputStream != null)
                mOutputStream.close();
        } catch (IOException iOException) {
            iOException.printStackTrace();
        }

        mFileDescriptor = null;
        mInputStream = null;
        mOutputStream = null;
        mReadThread = null;
        mUsbAccessory = null;
        mInitialized = false;
        mReadBuffer.clear();
        Shared_Instance = null;
        System.exit(0);
    }

    public UsbAccessoryListerer getListener() {
        return this.mListener;
    }


    public UsbAccessory getUsbAccessory() {
        return this.mUsbAccessory;
    }

    public UsbManager getUsbManager() {
        return this.mUsbManager;
    }


    public boolean openAccessory() {
        mFileDescriptor = mUsbManager.openAccessory(mUsbAccessory);
        if (mFileDescriptor != null) {
            FileDescriptor fd = mFileDescriptor.getFileDescriptor();
            mInputStream = new FileInputStream(fd);
            mOutputStream = new FileOutputStream(fd);
            if (mInputStream == null || mOutputStream == null)
                return false;
            if (mReadThread == null) {
                mReadThread = new ReadThread(mReadThreadListener, mInputStream);
                mReadThread.start();
            }
            setDefaultConfig();
            mHandler.postDelayed(mSendCmdRunnable, 100L);
            reset();
            return true;
        }
        return false;


    }

    void reset() {
        synchronized (this.mReadBuffer) {
            mReadBuffer.clear();
            mCommands.clear();
            AckCmd ackCmd = new AckCmd();
            mCommands.add(ackCmd);
            GetSleepSettingCmd getSleepSettingCmd = new GetSleepSettingCmd();
            mCommands.add(getSleepSettingCmd);

            GetDeviceInfoCmd getDeviceInfoCmd = new GetDeviceInfoCmd();
            mCommands.add(getDeviceInfoCmd);

            GetErrorLogCmd getErrorLogCmd = new GetErrorLogCmd();
            mCommands.add(getErrorLogCmd);

            GetResistanceLevelRangeCmd getResistanceLevelRangeCmd = new GetResistanceLevelRangeCmd();
            mCommands.add(getResistanceLevelRangeCmd);

            GetWorkoutControlStateCmd stateCmd = new GetWorkoutControlStateCmd();
            mCommands.add(stateCmd);

            GetResistanceLevelCmd getResistanceLevelCmd = new GetResistanceLevelCmd();
            mCommands.add(getResistanceLevelCmd);

            WorkoutControlStateNotify workoutControlStateNotify = new WorkoutControlStateNotify();
            mCommands.add(workoutControlStateNotify);

            WorkoutStatusNotify workoutStatusNotify = new WorkoutStatusNotify();
            mCommands.add(workoutControlStateNotify);

            ResistanceLevelNotify resistanceLevelNotify = new ResistanceLevelNotify();
            mCommands.add(resistanceLevelNotify);
        }
    }

    public void setAsRowerMode() {
        synchronized (this.mReadBuffer) {
            mCommands.clear();
            GetSleepSettingCmd getSleepSettingCmd = new GetSleepSettingCmd();
            mCommands.add(getSleepSettingCmd);

            GetDeviceInfoCmd getDeviceInfoCmd = new GetDeviceInfoCmd();
            mCommands.add(getDeviceInfoCmd);

            GetErrorLogCmd getErrorLogCmd = new GetErrorLogCmd();
            mCommands.add(getErrorLogCmd);

            GetResistanceLevelRangeCmd getResistanceLevelRangeCmd = new GetResistanceLevelRangeCmd();
            mCommands.add(getResistanceLevelRangeCmd);

            GetWorkoutControlStateCmd stateCmd = new GetWorkoutControlStateCmd();
            mCommands.add(stateCmd);

            GetResistanceLevelCmd getResistanceLevelCmd = new GetResistanceLevelCmd();
            mCommands.add(getResistanceLevelCmd);

            WorkoutControlStateNotify workoutControlStateNotify = new WorkoutControlStateNotify();
            mCommands.add(workoutControlStateNotify);

            WorkoutStatusNotify workoutStatusNotify = new WorkoutStatusNotify();
            mCommands.add(workoutControlStateNotify);

            ResistanceLevelNotify resistanceLevelNotify = new ResistanceLevelNotify();
            mCommands.add(resistanceLevelNotify);


        }
    }

    public void setConfig(int baud, byte dataBits, byte stopBits, byte parity, byte flowControl) {
        mWriteDataBuffer[0] = (byte) baud;
        mWriteDataBuffer[1] = (byte) (baud >> 8);
        mWriteDataBuffer[2] = (byte) (baud >> 16);
        mWriteDataBuffer[3] = (byte) (baud >> 24);
        mWriteDataBuffer[4] = dataBits;
        mWriteDataBuffer[5] = stopBits;
        mWriteDataBuffer[6] = parity;
        mWriteDataBuffer[7] = flowControl;
        sendPacket(8);
    }

    public void setDefaultConfig() {
        setConfig(9600, (byte) 8, (byte) 1, (byte) 0, (byte) 0);
    }

    public void setListener(@NonNull UsbAccessoryListerer paramUsbAccessoryListerer) {
        mListener = paramUsbAccessoryListerer;
    }


    public void setUsbAccessory(UsbAccessory paramUsbAccessory) {
        mUsbAccessory = paramUsbAccessory;
    }

    public void setUsbManager(UsbManager paramUsbManager) {
        this.mUsbManager = paramUsbManager;
    }


    void writeValue(@NonNull Command paramCommand) {
        byte[] arrayOfByte = paramCommand.compactRequestData().bytes();
        writeValue(arrayOfByte);
    }

    void writeValue(@NonNull byte[] paramArrayOfbyte) {
        FileOutputStream fileOutputStream = mOutputStream;
        if (fileOutputStream == null)
            return;
        try {
            fileOutputStream.write(paramArrayOfbyte);
            ADLog.i(TAG, "SND %s", new Object[]{ADConverter.byteArrayToHexString(paramArrayOfbyte)});
        } catch (
                IOException exception) {
            exception.printStackTrace();
        }
    }
}


/* Location:              /home/sakura/work/test/dex2jar-2.1/dex-tools-2.1-SNAPSHOT/classes-dex2jar.jar!/changyow/santoble/usb_accessory/UsbAccessoryManager.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */