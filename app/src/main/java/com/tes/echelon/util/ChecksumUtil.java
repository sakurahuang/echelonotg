package com.tes.echelon.util;

public class ChecksumUtil {
    public static byte calcChecksum(byte[] paramArrayOfbyte) {
        return calcChecksum(paramArrayOfbyte, paramArrayOfbyte.length);
    }

    public static byte calcChecksum(byte[] paramArrayOfbyte, int paramInt) {
        int i = paramInt;
        if (paramInt > paramArrayOfbyte.length)
            i = paramArrayOfbyte.length;
        byte b = 0;
        paramInt = 0;
        while (b < i) {
            try {
                paramInt += paramArrayOfbyte[b];
                b++;
            } catch (Exception e) {
            }
        }
        return (byte) (paramInt & 0xFF);
    }

    public static boolean verifyChecksum(byte[] paramArrayOfbyte) {
        int i = paramArrayOfbyte.length;
        boolean bool = true;
        if (calcChecksum(paramArrayOfbyte, i - 1) != paramArrayOfbyte[paramArrayOfbyte.length - 1])
            bool = false;
        return bool;
    }
}


/* Location:              /home/sakura/work/test/dex2jar-2.1/dex-tools-2.1-SNAPSHOT/classes-dex2jar.jar!/changyow/santoble/util/ChecksumUtil.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */