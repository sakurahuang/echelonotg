package com.tes.echelon.listener;

import java.util.Date;

public interface BLEPeripheralListener extends FactorySettingsListener {
  void controlStateChanged(int paramInt);
  
  void onAckResponse();
  
  void onGetControlStateResponse(int paramInt);
  
  void onGetDeviceInfoResponse(int paramInt, String paramString1, String paramString2);
  
  void onGetErrorLogResponse(byte[] paramArrayOfbyte);
  
  void onGetFTMPValidDateResponse(Date paramDate);
  
  void onGetResistanceLevelRangeResponse(int paramInt1, int paramInt2);
  
  void onGetResistanceLevelResponse(int paramInt);
  
  void peripheralInitialized();
  
  void resistanceLevelChanged(int paramInt);
  
  void workoutStatusChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4, double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4);
  
  void workoutStatusChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, double paramDouble1, double paramDouble2, int paramInt6, double paramDouble3, double paramDouble4);
}


/* Location:              /home/sakura/work/test/dex2jar-2.1/dex-tools-2.1-SNAPSHOT/classes-dex2jar.jar!/changyow/santoble/BLEPeripheralListener.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */