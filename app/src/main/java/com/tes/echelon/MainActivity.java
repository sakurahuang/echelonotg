package com.tes.echelon;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbAccessory;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tes.echelon.adapter.RecyclerAdapter;
import com.tes.echelon.usb_accessory.UsbAccessoryListerer;
import com.tes.echelon.usb_accessory.UsbAccessoryManager;
import com.tes.echelon.util.AppUtil;
import com.tes.echelon.util.Workout;
import com.tes.echelon.utility.ADConverter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity implements UsbAccessoryListerer {

    private UsbManager usbManager;
    private static final String ACTION_USB_PERMISSION = "com.UARTLoopback.USB_PERMISSION";
    public PendingIntent mPermissionIntent;
    private TextView txvStatus;
    private TextView txvResponse;

    public static String ManufacturerString = "mManufacturer=FTDI";
    public static String ModelString2 = "mModel=Android Accessory FT312D";
    public static String VersionString = "mVersion=1.0";
    private UsbAccessory usbAccessory;
    private UsbAccessoryManager mUsbAccessoryManager;
    private boolean mPermissionRequestPending = false;
    private RecyclerView recyclerView;
    private int controlState = 0;
    private int time = 0;
    private boolean asc = true;
    boolean bHasUSBPermission = false;
    double calories = 0.0d;
    int consume = 0;
    int count = 0;
    int cycle = 0;
    int direction = 0;
    double distance = 0.0d;
    int hr = 0;
    int incline = 0;
    int level = 0;
    int time500 = 0;
    int rpm = 0;
    double speed = 0.0d;
    double watt = 0.0d;
    int timerCount = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (!AppUtil.isMTB())
            finish();
        txvStatus = findViewById(R.id.txvStatus);
        txvResponse = findViewById(R.id.txvResponse);
        initView();
        initUsb();
        displayValues();
    }

    private void displayValues() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(Looper.myLooper().toString());
        stringBuffer.append(Thread.currentThread().toString());
        Log.i("tag", stringBuffer.toString());
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                doDisplayValues();
            }
        });
    }

    private void doDisplayValues() {
        String str1;
        if (controlState == 1) {
            str1 = "Start";
        } else if (controlState == 0) {
            str1 = "Stop";
        } else {
            str1 = String.format("%x", new Object[]{Integer.valueOf(controlState)});
        }
        String str2 = String.format("%2d:%2d:%2d", new Object[]{Integer.valueOf(time / 3600), Integer.valueOf(time % 3600 / 60), Integer.valueOf(time % 60)});
        if (Workout.sharedInstance().isRower()) {
            str1 = String.format("Control State: %s\nResistance Level: %d\nTime: %s\t\tstroke: %d\nSPM: %03d\t\tHR: %03d\nSpeed: %02.02f\t\tDistance: %03.02f\nTime/500: %d:%d\t\tDirection:%d\nCalories: %04.01f\t\tWatt: %03.01f", new Object[]{
                    str1, Integer.valueOf(this.level), str2, Integer.valueOf(this.count), Integer.valueOf(this.rpm), Integer.valueOf(this.hr), Double.valueOf(this.speed), Double.valueOf(this.distance), Integer.valueOf(this.time500 / 60), Integer.valueOf(this.time500 % 60),
                    Integer.valueOf(this.direction), Double.valueOf(this.calories), Double.valueOf(this.watt)});
        } else if (Workout.sharedInstance().isTreadmill()) {
            str1 = String.format("Control State: %s\nIncline: %d\nTime: %s\t\tHR: %03d\nSpeed: %02.02f\t\tDistance: %03.02f\nCalories: %04.01f\t\tWatt: %03.01f", new Object[]{str1, Integer.valueOf(this.incline), str2, Integer.valueOf(this.hr), Double.valueOf(this.speed), Double.valueOf(this.distance), Double.valueOf(this.calories), Double.valueOf(this.watt)});
        } else {
            str1 = String.format("Control State: %s\nResistance Level: %d\nTime: %s\t\tcount: %d\nRPM: %03d\t\tHR: %03d\nSpeed: %02.02f\t\tDistance: %03.02f\nCalories: %04.01f\t\tWatt: %03.01f", new Object[]{str1, Integer.valueOf(this.level), str2, Integer.valueOf(this.count), Integer.valueOf(this.rpm), Integer.valueOf(this.hr), Double.valueOf(this.speed), Double.valueOf(this.distance), Double.valueOf(this.calories), Double.valueOf(this.watt)});
        }
        str2 = str1;
        if (this.timerCount > 0)
            str2 = String.format("%s\nCycle: %d, totalTime:%d:%d:%d", new Object[]{str1, Integer.valueOf(this.cycle), Integer.valueOf(this.timerCount / 3600), Integer.valueOf(this.timerCount % 3600 / 60), Integer.valueOf(this.timerCount % 60)});

        txvStatus.setText(str2);

    }


    private void initView() {
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

    }


    @Override
    public void onReadErrorOccur() {

    }

    @Override
    public void workoutStatusChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4, double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4) {
        this.time = paramInt1;
        this.count = paramInt2;
        this.rpm = paramInt3;
        this.hr = paramInt4;
        this.speed = paramDouble1;
        this.distance = paramDouble2;
        this.calories = paramDouble3;
        this.watt = paramDouble4;
        displayValues();
    }

    @Override
    public void controlStateChanged(int paramInt) {
        this.controlState = paramInt;
        displayValues();
    }

    @Override
    public void onAckResponse() {

    }

    @Override
    public void onGetControlStateResponse(int paramInt) {
        if (paramInt == 1) {
            setResponseMessage("Control State: Start");
        } else if (paramInt == 0) {
            setResponseMessage("Control State: Stop");
        } else if (paramInt == 2) {
            setResponseMessage("Control State: Pause");
        } else {
            setResponseMessage(String.format("Control State: %x", new Object[]{Integer.valueOf(paramInt)}));
        }
    }

    @Override
    public void onGetDeviceInfoResponse(int paramInt, String paramString1, String paramString2) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                txvResponse.setText(String.format("HW Version:%s\nFW Version:%s", new Object[]{paramString1, paramString2}));
            }
        });

    }

    private void setResponseMessage(String messag) {
        if (TextUtils.isEmpty(messag))
            return;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                txvResponse.setText(messag);
            }
        });
    }

    @Override
    public void onGetErrorLogResponse(byte[] paramArrayOfbyte) {
        setResponseMessage(String.format("Error Codes:%s", new Object[]{ADConverter.byteArrayToHexString(paramArrayOfbyte)}));
    }

    @Override
    public void onGetFTMPValidDateResponse(Date paramDate) {
        setResponseMessage(String.format("FTMP Valid Date: %s", new Object[]{paramDate.toGMTString()}));

    }

    @Override
    public void onGetResistanceLevelRangeResponse(int paramInt1, int paramInt2) {
        setResponseMessage(String.format("Max Level:%d\nMin Level:%d", new Object[]{Integer.valueOf(paramInt1), Integer.valueOf(paramInt2)}));
    }

    @Override
    public void onGetSleepSettingResponse(int paramInt) {
        setResponseMessage(String.format("Sleep Setting: %d", new Object[]{Integer.valueOf(paramInt)}));

    }

    @Override
    public void onGetResistanceLevelResponse(int paramInt) {
        setResponseMessage(String.format("Resistance Level: %d", new Object[]{Integer.valueOf(paramInt)}));
    }

    @Override
    public void peripheralInitialized() {
        showControlPane();

    }

    private void showControlPane() {
        runOnUiThread(new Runnable() {
            public void run() {
                List<String> arrayList = new ArrayList();
                arrayList.add("get Device Info");
                arrayList.add("get Error Log");
                arrayList.add("get Resistance Level Range");
                arrayList.add("get Resistance Level");
                arrayList.add("get Workout Control State");
                arrayList.add("Stop Workout");
                arrayList.add("Start Workout");
                arrayList.add("Increase Resistance Level");
                arrayList.add("Decrease Resistance Level");
                arrayList.add("RPM Ratio 1");
                arrayList.add("RPM Ratio 4.4");
                arrayList.add("Get Sleep Setting");
                arrayList.add("Set Sleep Setting: off/0");
                arrayList.add("Set Sleep Setting: on/1");
                RecyclerAdapter adapter = new RecyclerAdapter(arrayList);
                adapter.setListener(new RecyclerAdapter.ItemClickListener() {
                    @Override
                    public void click(int position) {
                        if (mUsbAccessoryManager == null)
                            return;
                        switch (position) {
                            case 0://getDevices info
                                mUsbAccessoryManager.getDeviceInfo();
                                break;
                            case 1://getError
                                mUsbAccessoryManager.getErrorLog();
                                break;
                            case 2:
                                mUsbAccessoryManager.getResistanceLevelRange();
                                break;
                            case 3:
                                mUsbAccessoryManager.getResistanceLevel();
                                break;
                            case 4:
                                mUsbAccessoryManager.getWorkoutControlState();
                                break;
                            case 5:
                                mUsbAccessoryManager.stopWorkout();
                                break;
                            case 6:
                                mUsbAccessoryManager.startWorkout();
                                break;
                            case 7:
                                mUsbAccessoryManager.setResistanceLevel(level + 1);
                                break;
                            case 8:
                                mUsbAccessoryManager.setResistanceLevel(level - 1);
                                break;
                            case 9:
                                Workout.setRpmRatio(1.0d);
                                break;
                            case 10:
                                Workout.setRpmRatio(4.4d);
                                break;
                            case 11:
                                mUsbAccessoryManager.getSleepSetting();
                                break;
                            case 12:
                                mUsbAccessoryManager.setSleepSetting(0);
                                break;
                            case 13:
                                mUsbAccessoryManager.setSleepSetting(1);
                                break;
                        }
                    }
                });
                recyclerView.setAdapter(adapter);
            }
        });
    }

    @Override
    public void workoutStatusChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, double paramDouble1, double paramDouble2, int paramInt6, double paramDouble3, double paramDouble4) {
        this.time = paramInt1;
        this.count = paramInt3;
        this.rpm = paramInt4;
        this.hr = paramInt5;
        this.speed = paramDouble1;
        this.distance = paramDouble2;
        this.calories = paramDouble3;
        this.watt = paramDouble4;
        this.time500 = paramInt6;
        this.direction = paramInt2;
        displayValues();
    }

    @Override
    public void resistanceLevelChanged(int paramInt) {
        this.level = paramInt;
        displayValues();
    }


    private void initUsb() {
        usbManager = (UsbManager) getSystemService(Context.USB_SERVICE);
        mPermissionIntent = PendingIntent.getBroadcast(this, 0, new Intent(ACTION_USB_PERMISSION), 0);

        IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_USB_PERMISSION);
        filter.addAction(UsbManager.ACTION_USB_ACCESSORY_DETACHED);
        registerReceiver(mUsbReceiver, filter);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mUsbReceiver != null) {
            unregisterReceiver(mUsbReceiver);
        }
        if (mUsbAccessoryManager != null) {
            mUsbAccessoryManager.destoryAccessory(true);
        }
        mUsbAccessoryManager = null;
    }

    @Override
    protected void onResume() {
        super.onResume();
        resumeAccessory();
    }

    private void resumeAccessory() {
        UsbAccessory[] accessories = usbManager.getAccessoryList();

        if (accessories != null) {
            Toast.makeText(this, "Accessory Attached", Toast.LENGTH_SHORT).show();
        } else {
            // return 2 for accessory detached case
            //Log.e(">>@@","ResumeAccessory RETURN 2 (accessories == null)");
            return;
        }
        UsbAccessory accessory = (accessories == null ? null : accessories[0]);
        if (accessory != null) {
            if (-1 == accessory.toString().indexOf(ManufacturerString)) {
                Toast.makeText(this, "Manufacturer is not matched!", Toast.LENGTH_SHORT).show();
                return;
            }

            if (-1 == accessory.toString().indexOf(ModelString2)) {
                Toast.makeText(this, "Model is not matched!", Toast.LENGTH_SHORT).show();
                return;
            }

            if (-1 == accessory.toString().indexOf(VersionString)) {
                Toast.makeText(this, "Version is not matched!", Toast.LENGTH_SHORT).show();
                return;
            }

            Toast.makeText(this, "Manufacturer, Model & Version are matched!", Toast.LENGTH_SHORT).show();

            if (usbManager.hasPermission(accessory)) {
                OpenAccessory(accessory);
            } else {
                synchronized (mUsbReceiver) {
                    if (!mPermissionRequestPending) {
                        Toast.makeText(this, "Request USB Permission", Toast.LENGTH_SHORT).show();
                        usbManager.requestPermission(accessory,
                                mPermissionIntent);
                        mPermissionRequestPending = true;
                    }
                }
            }
        }
    }

    boolean isOpen;

    private void OpenAccessory(UsbAccessory accessory) {
        this.usbAccessory = accessory;
        mUsbAccessoryManager = UsbAccessoryManager.createInstance(usbManager, usbAccessory);
        mUsbAccessoryManager.setListener(this);
        isOpen = mUsbAccessoryManager.openAccessory();
        if (!isOpen) {
            Toast.makeText(this, "usb accessory device open failure,please check the connected", Toast.LENGTH_SHORT).show();
            finish();
            return;
        }
    }

    /***********USB broadcast receiver*******************************************/
    public BroadcastReceiver mUsbReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (ACTION_USB_PERMISSION.equals(action)) {
                synchronized (this) {
                    UsbAccessory accessory = (UsbAccessory) intent.getParcelableExtra(usbManager.EXTRA_ACCESSORY);
                    if (intent.getBooleanExtra(usbManager.EXTRA_PERMISSION_GRANTED, false)) {
                        Toast.makeText(MainActivity.this, "Allow USB Permission", Toast.LENGTH_SHORT).show();
                        OpenAccessory(accessory);
                    } else {
                        Toast.makeText(MainActivity.this, "Deny USB Permission", Toast.LENGTH_SHORT).show();
                        Log.d("LED", "permission denied for accessory " + accessory);

                    }
                    mPermissionRequestPending = true;
                }
            } else if (usbManager.ACTION_USB_ACCESSORY_DETACHED.equals(action)) {
                if (mUsbAccessoryManager != null) {
                    mUsbAccessoryManager.destoryAccessory(true);
                }
            } else {
                Log.d("LED", "....");
            }
        }
    };


}