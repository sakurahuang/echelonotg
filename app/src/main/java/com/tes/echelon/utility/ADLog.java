package com.tes.echelon.utility;

import android.util.Log;

public class ADLog {
  public static boolean isLoggable = true;

  static int mLogLevel = 3;

  static final int mMinLogLevel = 2;

  public static void d(String paramString1, String paramString2, Object... paramVarArgs) {
    if (isLoggable && 3 >= mLogLevel) {
      String[] arrayOfString = spliteLogString(String.format(paramString2, paramVarArgs));
      int i = arrayOfString.length;
      for (byte b = 0; b < i; b++)
        Log.d(paramString1, arrayOfString[b]);
    }
  }

  public static void e(String paramString1, String paramString2, Object... paramVarArgs) {
    if (isLoggable && 6 >= mLogLevel) {
      String[] arrayOfString = spliteLogString(String.format(paramString2, paramVarArgs));
      int i = arrayOfString.length;
      for (byte b = 0; b < i; b++)
        Log.e(paramString1, arrayOfString[b]);
    }
  }

  public static void i(String paramString1, String paramString2, Object... paramVarArgs) {
    if (isLoggable && 4 >= mLogLevel) {
      String[] arrayOfString = spliteLogString(String.format(paramString2, paramVarArgs));
      int i = arrayOfString.length;
      for (byte b = 0; b < i; b++)
        Log.i(paramString1, arrayOfString[b]);
    }
  }

  public static void setLogLevel(int paramInt) {
    if (paramInt < 2) {
      mLogLevel = 2;
    } else {
      mLogLevel = paramInt;
    }
  }

  private static String[] spliteLogString(String paramString) {
    int k = paramString.length();
    int j = k / 4056;
    int i = j;
    if (k % 4056 != 0)
      i = j + 1;
    String[] arrayOfString = new String[i];
    for (j = 0; j < i; j++) {
      int i2 = j * 4056;
      int n = j + 1;
      int i1 = n * 4056;
      int m = i1;
      if (i1 > k)
        m = i2 + k - i2;
      arrayOfString[j] = paramString.substring(i2, m);
    }
    return arrayOfString;
  }

  public static void v(String paramString1, String paramString2, Object... paramVarArgs) {
    if (isLoggable && 2 >= mLogLevel) {
      String[] arrayOfString = spliteLogString(String.format(paramString2, paramVarArgs));
      int i = arrayOfString.length;
      for (byte b = 0; b < i; b++)
        Log.v(paramString1, arrayOfString[b]);
    }
  }

  public static void w(String paramString1, String paramString2, Object... paramVarArgs) {
    if (isLoggable && 5 >= mLogLevel) {
      String[] arrayOfString = spliteLogString(String.format(paramString2, paramVarArgs));
      int i = arrayOfString.length;
      for (byte b = 0; b < i; b++)
        Log.w(paramString1, arrayOfString[b]);
    }
  }
}


/* Location:              /home/sakura/work/test/dex2jar-2.1/dex-tools-2.1-SNAPSHOT/classes-dex2jar.jar!/appdevice/adble/utility/ADLog.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */