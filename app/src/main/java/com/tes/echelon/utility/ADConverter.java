package com.tes.echelon.utility;

import java.util.UUID;

public class ADConverter {
  public static UUID UUIDfrom16BitString(String paramString) {
    if (paramString != null) {
      if (paramString.length() == 4)
        return UUID.fromString(String.format("0000%s-0000-1000-8000-00805f9b34fb", new Object[] { paramString })); 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Invalid UUID: ");
      stringBuilder.append(paramString);
      throw new IllegalArgumentException(stringBuilder.toString());
    } 
    throw new NullPointerException("uuid16 == null");
  }
  
  public static String byteArrayToHexString(byte[] paramArrayOfbyte) {
    StringBuilder stringBuilder = new StringBuilder(paramArrayOfbyte.length * 2);
    int i = paramArrayOfbyte.length;
    try {
      for (byte b = 0; b < i; b++) {
        stringBuilder.append(String.format("%02X", new Object[] { Integer.valueOf(paramArrayOfbyte[b] & 0xFF) }));
      }
    }catch (Exception e){

    }

    return stringBuilder.toString();
  }
  
  public static byte[] hexStringToByteArray(String paramString) {
    int i = paramString.length();
    byte[] arrayOfByte = new byte[i / 2];
    for (byte b = 0; b < i; b += 2)
      arrayOfByte[b / 2] = (byte)(byte)((Character.digit(paramString.charAt(b), 16) << 4) + Character.digit(paramString.charAt(b + 1), 16)); 
    return arrayOfByte;
  }
}


/* Location:              /home/sakura/work/test/dex2jar-2.1/dex-tools-2.1-SNAPSHOT/classes-dex2jar.jar!/appdevice/adble/utility/ADConverter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */